package org.learn.qa4.sort;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.learn.qa4.Animal;
import org.learn.qa4.Cat;
import org.learn.qa4.HomeFish;
import org.learn.qa4.Piranha;

class SortingTest {

  public static Stream<Arguments> test() {
    return Stream.of(
        Arguments.of(
            new Animal[] {
                new Cat("Snowy", LocalDate.now()),
                new Cat("Kitten", LocalDate.now()),
                new HomeFish("Goldy", LocalDate.now()),
                new Piranha("Quiet Girl", LocalDate.now()),
                new Cat("Fluffy", LocalDate.now()),
                new Cat("Softy", LocalDate.now()),
                new Cat("Snowy", LocalDate.now())
            },
            new Animal[] {
                new Cat("Fluffy", LocalDate.now()),
                new HomeFish("Goldy", LocalDate.now()),
                new Cat("Kitten", LocalDate.now()),
                new Piranha("Quiet Girl", LocalDate.now()),
                new Cat("Snowy", LocalDate.now()),
                new Cat("Snowy", LocalDate.now()),
                new Cat("Softy", LocalDate.now())
            },
            new SelectionSort<Animal>()
        ),
        Arguments.of(
            new Integer[]{1,5,6,2,4,3},
            new Integer[]{1,2,3,4,5,6},
            new SelectionSort<Integer>()
        ),
        Arguments.of(
            new Integer[]{5,6,2,4,3,1},
            new Integer[]{1,2,3,4,5,6},
            new BubbleSort<Integer>()
        ),
        Arguments.of(
            new String[]{"1","15","6","2","4","3"},
            new String[]{"1","15","2","3","4","6"},
            new SelectionSort<String>()
        )
    );
  }

  @ParameterizedTest
  @MethodSource
  <T extends Comparable<T>> void test(T[] input, T[] expected, Sort<T> sortMethod) {
    assertFalse(sortMethod.isAscending(input));

    sortMethod.sort(input);

    assertTrue(sortMethod.isAscending(input));
    assertThat(input).containsExactly(expected);
  }

  @Test
  void testUnhappyPath() {
    final var input = new Integer[]{null,1,5,6,2,4,3};
    final var expected = new Integer[]{null,1,5,6,2,4,3};

    new SelectionSort<Integer>().sort(input);

    assertThat(input).containsExactly(expected);
  }

}