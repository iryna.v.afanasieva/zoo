package org.learn.qa4.wrappers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WrapperShow {
  private static final Logger log = LogManager.getLogger(WrapperShow.class);
  public static void main(String[] args) {
    Boolean incorrectUsage = new Boolean("");
    var correctUsage = Boolean.parseBoolean("truu");
    log.info("Incorrect usage: {} VS correct usage: {}", incorrectUsage, correctUsage);

    log.info("Input list form string: {}",
        Arrays.stream("true;false;truu;True;TRUE;TrUe".split(";"))
            .map(Boolean::parseBoolean)
            .toList());

    Float incorrectFloat = new Float(1.0);
    var correctFloat = Float.valueOf((float) 45.);

    log.info("FLOAT:\n Incorrect usage: {} VS correct usage: {}", incorrectFloat, correctFloat);

    log.info("Input list from string: {}",
        Arrays.stream("1.2;42;5.0;8".split(";"))
            .map(Float::valueOf)
            .toList()
    );

    String inputFloat = "Not A Float";
    try {
      log.info("Incorrect string value {}", Float.valueOf(inputFloat));
    } catch(Throwable throwable) {
      log.error(throwable.getMessage(), throwable);
    }

//    Character character = Character.valueOf("cat"); // Ctrl + '/'

    Boolean falseValue = Boolean.valueOf("false");// False; falseeeee

    // Integer, Boolean, Float ...
//    Integer intValue = generateIntValue();
//    if (intValue == null) {
//      log.info("Ups...");
//      throw new IllegalArgumentException("Null int value detected.");
//    }
//    int intPrimitive = intValue;
//    log.info("Lucky int primitive: {}", intPrimitive);

    MathContext mc = new MathContext(6, RoundingMode.HALF_UP);
    BigDecimal bigDecimal = new BigDecimal("42.123456", mc);
    log.info("big decimal is {}", bigDecimal);

    Integer valOne = 127;
    Integer valTwo = 127;
    Integer valThree = 420;
    Integer valFour = 420;

    log.info("valOne == valTwo is {}", valOne == valTwo);
    log.info("valThree == valFour is {}", valThree == valFour);

    BigInteger bigInteger = new BigInteger("123");
    BigInteger newInteger = new BigInteger("124");
    log.info("big integer: {}", bigInteger);
    log.info("bigInteger < newInteger: {}", bigInteger.compareTo(newInteger) < 0);
    log.info("newInteger < bigInteger: {}", newInteger.compareTo(bigInteger) < 0);
  }

  private static Integer generateIntValue() {
    Random random = new Random();
    if (random.nextInt(1000) % 2 == 0) {
      return random.nextInt(1000);
    }
    return null;
  }
}
