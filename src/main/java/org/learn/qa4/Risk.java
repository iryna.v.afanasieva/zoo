package org.learn.qa4;

import java.lang.annotation.Repeatable;

@Repeatable(value=Risks.class)
public @interface Risk {
  Danger danger(); //enum
  int level() default 1; // primitives
  String description() default "No Description"; // String
  Class clazz() default Animal.class; // Class
}
