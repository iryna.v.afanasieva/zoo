package org.learn.qa4;

import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StringShow {
  private static final Logger log = LogManager.getLogger(StringShow.class);

  public static void main(String[] args) {
    String hello = "Hello";
    String helloWorld = "Hello World!";
    Object helloObject = "H" + "ello";
    String ss = helloWorld.substring(0, helloWorld.indexOf(" ")).intern();

    log.info(String.format("%s == %s -> %s", helloObject, hello, helloObject == hello));
    log.info(String.format("%s == %s -> %s", hello, ss, hello == ss));

    log.info(String.format("%s.equals(%s) -> %s", hello, ss, hello.equals(ss)));

    final var matcher = Pattern.compile("((\\+|-)?[1-9][0-9]*)|0").matcher("input");
    matcher.replaceAll("");

    String str = new String("d1fillss12345");
    char[] dst = new char[str.length()];
    str.getChars(0, str.length(), dst, 0);
    for (char ch : dst) {
      if (Character.isDigit(ch)) {
        log.info(ch);
      }
    }
    log.info(matcher);
    log.info(String.format("cat ? dog = %s", "cat".compareTo("dog")));
    log.info(String.format("cat ? Dog = %s", "cat".compareToIgnoreCase("dog")));

    log.info(String.format("[%s]", " wwelk wel   ".trim()));
    log.info("".isBlank());
  }
}
