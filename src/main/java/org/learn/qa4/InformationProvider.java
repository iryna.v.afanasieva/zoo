package org.learn.qa4;

import java.util.Arrays;

public class InformationProvider {
  public static String getRisksInfo(Animal animal) {
    if (animal == null) {
      return "No animal provided";
    }
    if (!animal.getClass().isAnnotationPresent(Risks.class)) {
      return "No information available";
    }
    final var risks = animal.getClass().getAnnotation(Risks.class);
    return Arrays.stream(risks.value())
        .map(risk -> String.format("%s(%d) ", risk.danger(), risk.level()))
        .reduce("", String::concat);
  }
}
