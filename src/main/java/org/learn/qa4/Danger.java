package org.learn.qa4;

public enum Danger {
  AGGRESSIVE, LOUD, VENOMOUS
}
