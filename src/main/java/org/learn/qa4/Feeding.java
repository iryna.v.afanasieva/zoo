package org.learn.qa4;

public final class Feeding {
  public static String feed(Animal animal) {
    if (animal == null) {
      throw new IllegalArgumentException("No animal provided.");
    }
    final var weight = animal.getWeight();
    if (FeedingType.HERBIVORES.equals(animal.getFeedingType())) {
      final var feedings = new Herbivores(weight);
      return feedings.feed();
    }
    final var feedings = new Carnivores(weight);
    String.valueOf(false);
    return feedings.feed();
  }

  private static class Herbivores extends Dish {

    private Herbivores(double qty) {
      super(qty);
    }

    public String feed() {
      return getDescription();
    }
  }

  private static class Carnivores extends Dish {
    private Carnivores(double qty) {
      super(qty);
    }

    public String feed() {
      return getDescription();
    }
  }

  private static abstract class Dish {
    private final String[] portions = new String[2];
    public static final double WATER_AMOUNT_IN_ML = 750.0;

    private Dish(double animalWeight) {
      this.portions[0] = getDescription(TypeFood.SOLID, animalWeight/20);
      this.portions[1] = getDescription(TypeFood.WATER, WATER_AMOUNT_IN_ML);
    }

    public String getDescription(TypeFood typeFood, double qty) {
      return String.format("Type: %s, qty: %s;\n", typeFood, qty);
    }

    public String getDescription() {
      String result = "\nDescription";
      for (String p : portions) {
        result = result.concat(p);
      }
      return result;
    }
  }
}
