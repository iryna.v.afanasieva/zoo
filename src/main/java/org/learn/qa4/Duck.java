package org.learn.qa4;

import java.time.LocalDate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Duck extends Animal implements MakeSound {
  private static final Logger log = LogManager.getLogger(Duck.class);
  private static String SOUND = "krya";
  protected Duck(String name, LocalDate bDate) {
    super(name, bDate);
    log.info("I'm the %sth animal in the Zoo. %n", Animal.getCount());
  }

  public String doSound() {
    return SOUND;
  }

  public String toString() {
    return super.toString()
        + " It says: " + doSound();
  }

}
