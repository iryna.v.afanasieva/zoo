package org.learn.qa4.optional;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.LongStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OptionalShow {
  private static final Logger log = LogManager.getLogger(OptionalShow.class);

  public static void main(String[] args) {
//    final var emptyOptional = Optional.empty();
//    final var nullOptional = Optional.ofNullable(null);
//    final var nonNullOptional = Optional.of("null");
//
//    log.info("empty optional {}", emptyOptional);
//    log.info("null optional {}", nullOptional);
//    log.info("non null optional {}", nonNullOptional);
//
//    log.info("is empty for empty optional {}", emptyOptional.isEmpty());
//    log.info("is empty for null optional {}", nullOptional.isEmpty());
//    log.info("is empty for non null optional {}", nonNullOptional.isEmpty());

    final String input = "; ;   ; ;  ; ;  ;;;cat;dog;duck;piranha;fish;owl;bird;";
    String resultOldSchool = null;
    if (input != null) {
      final var arrayStr = input.split(";");
      for (String string : arrayStr) {
        if (string != null && string.isBlank()) {
          continue;
        }
        resultOldSchool = string;
        break;
      }
    }
    log.info("Old School not null string search: {}", resultOldSchool);
    String inputNull = null;
    final var result = Optional.ofNullable(inputNull)
        .stream()
        .filter(Objects::nonNull)
        .map(item -> item.split(";"))
        .map(array -> Arrays.stream(array).toList())
        .flatMap(List::stream)
        .filter(Objects::nonNull)
        .peek(System.out::println)
        .filter(Predicate.not(String::isBlank))
        .findFirst();

    log.info("result is : {}, is empty: {}", result, result.isEmpty());

    final var optionalIntResult = LongStream
        .iterate(1, item -> item + 1)
        .limit(10)
        .findFirst();

    log.info("Iterate result: {}", optionalIntResult);

    List<String> list = new LinkedList<>();
    list.add("42");
    list.add("42.");
    list.add("42");
    log.warn("{}", list);
  }

}
