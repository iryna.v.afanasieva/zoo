package org.learn.qa4;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
  private static final Logger log = LogManager.getLogger(Main.class);

  public static void main(String[] args) {

    printCount();

    Animal[] animals = {
        new Cat("Kitten", LocalDate.now()),
        new Duck("Duckling", LocalDate.now()),
        new Duck("Duckling", LocalDate.now().minus(Period.ofDays(5))),
        new HomeFish("Goldy", LocalDate.now()),
        new Piranha("Quiet Girl", LocalDate.now()),
        null
    };

    printCount();

    List<? super Animal> animalList = new ArrayList<>();
    for (Animal animal : animals) {
      if (animal != null) {
        animalList.add(animal);
      }
    }
    /* Safe: because all parents can reference its children */
    List<? super Animal> animalObject = new ArrayList<Object>();

    animalObject.add(new Animal("Name", LocalDate.now()) {});

    List<String> immutableList = List.of("abc", "123", "453");

    mutateWildcard(immutableList);
    mutateWildcard(animalList);

    Arrays.sort(animals, (o1, o2) -> {
          if (o2 == o1) {
            return 0;
          }
          if (o1 == null || o2 == null) {
            return 0;
          }
          int nameCompare = o1.getName().compareTo(o2.getName());
          if (nameCompare != 0) {
            return nameCompare;
          }
          return o1.getBDate().compareTo(o2.getBDate());
        });

    for (Animal animal : animals) {
      if (animal != null) {
        log.info(animal);
        log.info(InformationProvider.getRisksInfo(animal));
        log.info(Feeding.feed(animal));
      }
    }

  }

  private static void mutateWildcard(List<?> immutableList) {
    if (immutableList == null) {
      log.warn("No list provided.");
      return;
    }
    immutableList.isEmpty();
    log.info("size {}", immutableList.size());
    final var nonNullElementsImmutableList = immutableList.stream()
        .filter(Objects::nonNull)
        .toList();
    log.info("size {}", nonNullElementsImmutableList.size());

    //immutableList.add(Integer.valueOf(123));
  }

  private static void printCount() {
    log.info("Animal count is: %s%n", Animal.getCount());
  }
}