package org.learn.qa4.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SortingShow {
  private static class NotSorted {}
  private static final Logger log = LogManager.getLogger(SortingShow.class);
  private static final int SIZE = 1_000_000;
  private static final int LIMIT = 5;
  private static final int MAX_VALUE = 100_000;

  public static void main(String[] args) {
    final List<Sortable<Integer>> sortables = new ArrayList<>();
    sortables.add(new BubbleSort<>());
    sortables.add(new SelectionSort<>());

    Integer[] randomArray = generateArray(SIZE);

    for(Sortable<Integer> sortable : sortables) {
      final var array = Arrays.copyOf(randomArray, SIZE);
      sortable.sort(array);
      show("Input:", randomArray);
      show("Output:", array);
    }

  }

  private static void show(String message, Integer[] input) {
    if (input == null || input.length == 0) {
      log.warn("Input is not provided.");
      return;
    }
    log.info("{} array (first {} of {}): {}",
        message,
        LIMIT,
        input.length,
        Arrays.stream(input)
            .limit(LIMIT)
            .toList()
    );
  }

  private static Integer[] generateArray(int size) {
    if (size <= 0) {
      size = SIZE;
    }
    final var array = new Integer[size];
    final var rand = new Random();

    for(int index = 0; index < size; index++) {
      array[index] = rand.nextInt(MAX_VALUE);
    }

    return array;
  }
}
