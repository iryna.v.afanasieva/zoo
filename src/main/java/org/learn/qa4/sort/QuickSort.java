package org.learn.qa4.sort;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class QuickSort<T extends Comparable<T>> extends Sort<T> {
  /* https://www.geeksforgeeks.org/quick-sort/ */
  private static final Logger log = LogManager.getLogger(QuickSort.class);

  @Override
  public void sort(T[] array) {
    throw new UnsupportedOperationException("Implement me, please!");
  }
}
