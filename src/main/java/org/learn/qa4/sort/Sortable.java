package org.learn.qa4.sort;

public interface Sortable<T extends Comparable<T>> {

  void sort(T[] array);
}
