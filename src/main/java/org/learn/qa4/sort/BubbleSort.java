package org.learn.qa4.sort;

public class BubbleSort<T extends Comparable<T>> extends Sort<T> {
  private static final String TITLE = "Bubble Sort";

  public BubbleSort() {
    title = TITLE;
  }

  @Override
  public void sort(T[] array) {
    sort(array, this::algorithm);
  }

  private void algorithm(T[] array) {
    if (array == null || array.length == 0) {
      log.warn("Array is not provided.");
      return;
    }
    /*

    9999  1  = 10000
    9998  2
    9997  3
    ...
    1      9999

    9999 * 10000 / 2 = 49995000

    O(n*n) = n(n-1)/2 + (1 .. n(n-1)/2) ~ n * (n - 1) = n*n => O(n*n)

    O(log2(n))

     */

    for (int outer = array.length - 1; outer >= 0; outer--) {
      for (int inner = 0; inner < outer; inner++) {
        countCompares++;
        if (array[inner] != null && array[inner].compareTo(array[inner + 1]) > 0) {
          Sort.swap(array, inner, inner + 1);
          countSwaps++;
        }
      }
    }
  }

}
