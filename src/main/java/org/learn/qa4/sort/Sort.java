package org.learn.qa4.sort;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Consumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Sort<T extends Comparable<T>> implements Sortable<T> {
  public static final long NANO_SECONDS_IN_SECOND = 1_000_000_000;
  public static final long MILLI_SECONDS_IN_SECOND = 1_000_000;

  protected static final Logger log = LogManager.getLogger(Sort.class);
  protected double countSwaps;
  protected double countCompares;
  protected long time;
  protected String title;

  static <P> boolean swap(P[] array, int i, int j) {
    if (array == null) {
      return false;
    }
    if (!valid(array.length, i) || !valid(array.length, j)) {
      return false;
    }
    if (i == j) {
      return true;
    }

    P temp = array[i];
    array[i] = array[j];
    array[j] = temp;

    return true;
  }

  static boolean valid(int length, int indexToCheck) {
    return indexToCheck > -1 && indexToCheck < length;
  }

  protected void sort(T[] array, Consumer<T[]> consumer) {
    if (array == null || array.length == 0) {
      log.warn("Array is not provided.");
      return;
    }
    final var count = Arrays.stream(array)
        .filter(Objects::nonNull)
        .count();

    if (count < array.length) {
      return;
    }

    initStatistics();

    long startTime = System.nanoTime();

    if (consumer != null) {
      consumer.accept(array);
    }

    long endTime = System.nanoTime();
    time = endTime - startTime;

    showStatistics(array);
  }

  public void showStatistics(T[] array) {
    log.info("================ {} ================", title);
    log.info(" swaps:\t\t\t{}", String.format("%.0f", countSwaps));
    log.info(" compares:\t\t{}", String.format("%.0f", countCompares));
    log.info(" time, ms:\t\t{}", time / MILLI_SECONDS_IN_SECOND);
    log.info(" time, s:\t\t{}", time / NANO_SECONDS_IN_SECOND);
    log.info(" is ascending:\t{}", isAscending(array));
    log.info("================================================");
  }

  public void initStatistics() {
    countSwaps = 0;
    countCompares = 0;
  }

  public boolean isAscending(T[] array) {
    if (array == null || array.length == 0) {
      return false;
    }
    for (int index = 1; index < array.length; index++) {
      if (array[index - 1] == null || array[index - 1].compareTo(array[index]) > 0) {
        return false;
      }
    }
    return true;
  }
}
