package org.learn.qa4.sort;

public class SelectionSort<T extends Comparable<T>> extends Sort<T> {
  private static final String SELECTION_SORT = "Selection Sort";

  public SelectionSort() {
    title = SELECTION_SORT;
  }

  @Override
  public void sort(T[] array) {
    sort(array, this::algorithm);
  }

  private void algorithm(T[] array) {
    if (array == null || array.length == 0) {
      log.warn("Array is not provided.");
      return;
    }

    for (int outer = 0; outer < array.length - 1; outer++) {
      for (int inner = outer + 1; inner < array.length; inner++) {
        countCompares++;
        if (array[outer] != null && array[outer].compareTo(array[inner]) > 0) {
          Sort.swap(array, outer, inner);
          countSwaps++;
        }
      }
    }
  }

}
