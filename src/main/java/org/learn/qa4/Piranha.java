package org.learn.qa4;

import java.time.LocalDate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Risks({
    @Risk(danger = Danger.LOUD),
    @Risk(danger = Danger.AGGRESSIVE, level = 10)
})
public class Piranha extends Fish {
  private static final Logger log = LogManager.getLogger(Piranha.class);
  public Piranha(String name, LocalDate bDate) {
    super(name, bDate);
  }

  @Override
  public String doSound() {
    log.info("Start doing sound...");
    try {
      firstMethod();
    } catch (RuntimeException exception) {
      log.warn("something goes wrong", exception);
      log.error("");
      return exception.getMessage();
    }
    log.info("End doing sound...");
    return "Ok!";
  }

  private void firstMethod() {
    secondMethod();
  }

  private void secondMethod() {
    throw new RuntimeException("Ops...");
  }
}
