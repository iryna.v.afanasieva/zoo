package org.learn.qa4;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Risks({
    @Risk(danger = Danger.LOUD)
})
public abstract class Animal implements Comparable<Animal> {
  private static final Logger log = LogManager.getLogger(Animal.class);
  private static final String FAVOURITE_FOOD = "UNKNOWN";
  private final FeedingType feedingType;
  private final double weight;

  public static String getDefaultFavouriteFood() {
    return FAVOURITE_FOOD;
  }
  private static int count;

  {
    String wow = "wow";
    log.info(wow);
  }

  private final String name;
  private final LocalDate bDate;

  private String favouriteFood;

  public String getName() {
    return name;
  }
  public LocalDate getBDate() {
    return bDate;
  }

  public void setFavouriteFood(String favouriteFood) {
    this.favouriteFood = favouriteFood;
  }

  public String getFavouriteFood() {
    return favouriteFood;
  }
  protected Animal(String name, LocalDate bDate) {
    this(FeedingType.HERBIVORES, name, bDate);
  }
  protected Animal(FeedingType feedingType, String name, LocalDate bDate) {
    if (feedingType == null) {
      throw new IllegalArgumentException("FeedingType should be not null");
    }
    if (name == null) {
      throw new IllegalArgumentException("Name should be not null");
    }
    if (bDate == null) {
      throw new IllegalArgumentException("Birth Date should be not null");
    }
    count++;
    this.feedingType = feedingType;
    this.name = name;
    this.bDate = bDate;
    this.favouriteFood = FAVOURITE_FOOD;
    this.weight = 150 - new Random().nextInt(100);
  }
  public static int getCount() {
    return count;
  }

  @Override
  public String toString() {
    return String.format("Name: %s, Birth Date: %s, Feeding Type: %s, Weight: %s",
        this.name, this.bDate, this.feedingType, this.weight);
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Animal animal = (Animal) o;
    return name.equals(animal.name) && bDate.equals(animal.bDate);
  }

  public int hashCode() {
    return Objects.hash(name, bDate);
  }

  public FeedingType getFeedingType() {
    return feedingType;
  }

  public double getWeight() {
    return weight;
  }

  @Override
  public int compareTo(Animal o) {
    if (o == null) {
      return 1;
    }
    if (o == this) {
      return 0;
    }

    int nameCompare = this.getName().compareTo(o.getName());
    if (nameCompare != 0) {
      return nameCompare;
    }
    return this.getBDate().compareTo(o.getBDate());
  }
}
