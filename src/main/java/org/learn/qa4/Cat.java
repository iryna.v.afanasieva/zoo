package org.learn.qa4;

import java.time.LocalDate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Cat extends Animal implements MakeSound {
  private static final Logger log = LogManager.getLogger(Cat.class);
  private static String SOUND = "meow";
  public Cat(String name, LocalDate bDate) {
    super(FeedingType.CARNIVORES, name, bDate);
    log.info("I'm the %sth animal in the Zoo. %n", Animal.getCount());
  }

  public String doSound() {
    return SOUND;
  }

  public String toString() {
    return super.toString()
        + " It says: " + doSound();
  }

}
