package org.learn.qa4;

public enum FeedingType {
  HERBIVORES, CARNIVORES;
}
