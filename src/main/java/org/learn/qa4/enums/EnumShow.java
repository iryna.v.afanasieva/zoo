package org.learn.qa4.enums;

import java.util.Arrays;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.learn.qa4.MakeSound;

public class EnumShow {
  private static final Logger log = LogManager.getLogger(EnumShow.class);

  public static void main(String[] args) {
    for(AnimalType type : AnimalType.values()) {
      log.info("{} sound is: {}", type, type.getSound());
    }
    testDoSound(null);
    testDoSound(AnimalType.values());
  }

  private static void testDoSound(MakeSound[] values) {
    if (values == null || values.length == 0) {
      log.warn("No values provided.");
      return;
    }
    Arrays.stream(values)
        .filter(Objects::nonNull)
        .forEach(item -> log.info(item.doSound()));
  }
}
