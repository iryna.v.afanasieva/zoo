package org.learn.qa4.enums;

import org.learn.qa4.MakeSound;

public enum AnimalType implements MakeSound {
  CAT("Meow"),
  PIRANHA("'Silence'"),
  DOG("Bark") {
    @Override
    public String doSound() {
      return "Overridden do sound method for MakeSound, DOG is barking!";
    }
  },
  DUCK("Quack");

  private final String sound;

  AnimalType(String sound) {
    this.sound = sound;
  }

  String getSound() {
    return sound;
  }

  @Override
  public String doSound() {
    return sound;
  }
}
