package org.learn.qa4;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WildcardShow {
  private static final Logger log = LogManager.getLogger(WildcardShow.class);
  private static class NoComparable {
    private final String title;

    private NoComparable(String title) {
      this.title = title;
    }
  }

  public static void main(String[] args) {
    NoComparable[] comparables = {new NoComparable("one"), new NoComparable("two")};
    log.info("Start!");
    Arrays.sort(comparables, (x,y) -> 0);
    Animal nullAnimal = null;
    List<Animal> animals = new ArrayList<>();

    animals.add(new Cat("c", LocalDate.now()));
    animals.add(new Piranha("p", LocalDate.now()));

    animals.add(null);
    List<Cat> cats = List.of(new Cat("cat", LocalDate.now()));
    List<Piranha> piranhas = List.of(new Piranha("cat", LocalDate.now()));

    testAnimal(animals);
    testAnimal(cats);
    testAnimal(piranhas);
  }

  private static void testAnimal(List<? extends Animal> list) {
    if (list == null || list.isEmpty()) {
      return;
    }
    list.stream()
        .filter(Objects::nonNull)
        .forEach(item -> log.info(item.getName()));
  }
}
