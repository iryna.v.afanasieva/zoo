# zoo

## Task

1. Create [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) with `write_repository` scope
2. Request Access
3. Clone repository to your local machine
```
git clone https://YOUR_TOKEN_NAME:YOUR_TOKEN_VALUE@gitlab.com/iryna.v.afanasieva/zoo.git
```
If you clone without your Personal Access Token
```
git remote set-url origin https://YOUR_TOKEN_NAME:YOUR_TOKEN_VALUE@gitlab.com/iryna.v.afanasieva/zoo.git
```
4. Create new branch, follow `feature/fix-username` pattern, lets assume that your username is `gitlab_user`:
```
git checkout -b feature/fix-gitlab_user
```
5. Implement sorting algorithms.
6. Commit changes:
```
git status
git add .
git status
git commit -m "Fix-gitlab_user: your changes description"
```
7. Push your branch into repository:
```
git push -u origin feature/fix-gitlab_user
```
8. Create MR (Merge Request)
